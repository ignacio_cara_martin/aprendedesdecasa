/* GET  page. */
router.get('/niveles', function(req, res) {
    var db = req.db;
    var collection = db.get('niveles');
    collection.find({},{},function(e,docs){
        res.render('niveles', {
            "niveles" : docs
        });
    });
});
