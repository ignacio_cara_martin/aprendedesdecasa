var express = require('express');
var app = express();
var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/database_aprendedesdecasa');
var url = require('url');
var port = parseInt(process.env.PORT, 10) || 8080;
var jwt = require('jwt-simple');
var bodyParser = require('body-parser');
var moment = require('moment');
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(express.static('public'))
app.listen(process.env.PORT || 8888);
app.set('jwtTokenSecret', 'polepo1234');
//function to use db mongo
app.use(function(req,res,next){
    req.db = db;
    next();
});


//function to get niveles from mongo
app.get('/niveles*', function (req, res) {
    var url_parts = url.parse(req.url, true);
    var urlCheck = url_parts.pathname;
    urlCheck = urlCheck.substring(9);
  	var db = req.db;
    var collection = db.get('niveles');
    collection.find({"rows":{"$elemMatch":{"asignaturas":{"$elemMatch":{"name":urlCheck}}}}}
        ,{},function(e,docs){
        res.json(docs);
    });
});



//function to get asignaturas from mongo
app.get('/asignaturas', function (req, res) {
    var db = req.db;
    var collection = db.get('asignaturas');
    collection.find({},{},function(e,docs){
        res.json(docs);
    });
});

//function to get videos from mongo
app.get('/videos*', function (req, res) {
    var url_parts = url.parse(req.url, true);
    var urlCheck = url_parts.pathname;
    urlCheck = urlCheck.substring(9);
    var db = req.db;
    var collection = db.get('videos');
    if (urlCheck == "visitados") {
        collection.find({},{"sort" : {'visualizaciones': -1} ,"limit": 10},function(e,docs){
            res.json(docs);
        });
    };

    if (urlCheck == "recientes") {
        collection.find({},{"limit": 10, "sort": {'dateInserted': -1}},function(e,docs){
            res.json(docs);
        });
    };

    if (urlCheck.indexOf("title") > -1) {
        collection.find({"searchName": urlCheck.substring(urlCheck.indexOf("title") + 6)},{},function(e,docs){
            res.json(docs);
        })
    };
});

app.post('/adduser', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.insert(req.body, function(err, result){
        res.send(
            (err === null) ? { msg: '' } : { msg: err }
        );
    });
});

app.post('/logginUser', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.findOne({"email":req.body.email},{},function(e,docs){
        if (e) { 
        // user not found 
        return res.sendStatus(401);
        }
        if (!docs) {
        // incorrect username
        return res.sendStatus(400);
        }
        if (docs.password != req.body.password) {
        // incorrect password
        return res.sendStatus(401);
        }
        // User has authenticated OK
        var expires = moment().add(7,'days').valueOf();
        var token = jwt.encode({
            iss: docs.id,
            exp: expires
        }, app.get('jwtTokenSecret'));
        res.json({
            token : token,
            expires: expires,
            user: docs
        });
    });
});

// // Define a middleware function to be used for every secured routes
// var auth = function (req, res, next) {
//   if (!req.isAuthenticated()) {
//     res.send(401);
//   } else {
//     next();
//   }
// }; 
// // route to log in
// app.post('/login', auth, function (req, res) {
//   res.send(req.user);
// }); 
