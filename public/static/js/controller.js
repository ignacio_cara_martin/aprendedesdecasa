//var webApp = angular.module('webApp',['ngRoute', 'ngResource']);
var webApp = angular.module('webApp',[]);

/*webApp.config(['$routeProvider', '$locationProvider', appConfig]); 

function appConfig($routeProvider) {
    $routeProvider
        .when('/#/',
        {
            controller: "general",
        })
        .otherwise({
            redirectto:'/'
        });
        $locationProvider.html5Mode(true);
};*/

function string_to_slug(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";
    for (var i=0, l=from.length ; i<l ; i++) {
    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-'); // collapse dashes

    return str;
}


var token = "";

if (window.localStorage.getItem('token')) {
    token = window.localStorage.getItem('token');
};

if (token) {
    $.ajaxSetup({
        headers: {
            'x-access-token': token
        }
    });
}

webApp.controller('general', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.mainPage = true;
    $scope.selectedTitle = "";
    $scope.entrarPage = false;
    $scope.registrarPage = false;
    if (token) {
        $scope.loged = true;
        $scope.user = JSON.parse(window.localStorage.getItem('user'));
    } else {
        $scope.loged = false;
    }
    

    $rootScope.$on('selectSideBar', function(event, props){
        $scope.selectedTitle = props.selectedTitle;
        if ($scope.selectedTitle != "undefined" && $scope.selectedTitle != "") {
            $scope.mainPage = false;
            $scope.entrarPage = false;
            $scope.registrarPage = false;
        }
    });

    $rootScope.$on('selectVideo', function(event, props){
        if (props.idVideo) {
            $scope.mainPage = false;
            $scope.entrarPage = false;
            $scope.registrarPage = false;
        }
    });

    $rootScope.$on('logginUserOk', function(event, props){
        $scope.loged = true;
        $scope.user = props.user;
        $scope.mainPage = true;
        $scope.entrarPage = false;
    });

    $scope.entrarClick = function() {
        $scope.entrarPage = true;
        $scope.mainPage = false;
        $scope.registrarPage = false;
        $scope.selectedTitle = "";
    };

    $scope.salirClick = function() {
        $scope.loged = false;
        localStorage.setItem('token',"");
        localStorage.setItem('user',"");
        $scope.user = {};
        $scope.entrarPage = false;
        $scope.mainPage = true;
        $scope.registrarPage = false;
        $scope.$emit('salirEvent', {
        })
    };

    $scope.registrarClick = function() {
        $scope.entrarPage = false;
        $scope.registrarPage = true;
        $scope.mainPage = false;
        $scope.selectedTitle = "";
        $scope.$emit('registrarClickEvent', {
        })
    };
}]);

webApp.controller('sideBar', ['$scope', '$rootScope', 'Network', function($scope, $rootScope, Network) {
    $scope.sideBar = {};
    $scope.selectedTitle = "";
    $scope.selectedTitleAsignatura = "";
    $scope.selectedTitleLevel = "";
    //peticion de las asignatuas
    Network.getUrl("/asignaturas").success(function(data){
               $scope.sideBar.asignaturas = data;
    });
    //funcion cuando seleccionas un nivel
    $scope.selectLevel = function (name, title) {
        if (name) {
            $scope.selectedTitleLevel = title + " " + name;
            $scope.selectedTitle = $scope.selectedTitleLevel + " " + $scope.selectedTitleAsignatura;
            $scope.$emit('selectSideBar', {
                selectedTitle: $scope.selectedTitle,
                level: $scope.selectedTitleLevel, 
                asignatura: $scope.selectedTitleAsignatura,
                levels: $scope.sideBar.niveles,
                asignaturas: $scope.sideBar.asignaturas
            })
        };
    };
    //funcion cuando seleccionas una asignatura
    $scope.selectAsignatura = function (name, title) {
        if (name) {
            //peticion de los niveles
            Network.getUrl("/niveles:" + string_to_slug(title + " " + name)).success(function(data){
                $scope.sideBar.niveles = [];
                $scope.sideBar.niveles[0] = data[0];
                var added = false;
                for (var i = 1; i < data.length; i++) {
                    added = false;
                    for (var j = 0; j < $scope.sideBar.niveles.length; j++) {
                        if ($scope.sideBar.niveles[j].title === data[i].title) {
                            $scope.sideBar.niveles[j].rows.push(data[i].rows[0]);
                            added = true;
                        }
                    }
                    if (!added) {
                        $scope.sideBar.niveles[$scope.sideBar.niveles.length] = data[i];
                    }
                }
                $scope.selectedTitleAsignatura = title + " " + name;
                $scope.selectedTitle = $scope.selectedTitleLevel + " " + $scope.selectedTitleAsignatura;
                $scope.$emit('selectSideBar', {
                    selectedTitle: $scope.selectedTitle,
                    level: $scope.selectedTitleLevel, 
                    asignatura: $scope.selectedTitleAsignatura,
                    levels: $scope.sideBar.niveles,
                    asignaturas: $scope.sideBar.asignaturas
                })
            });
        };
    };
}]);

webApp.controller('mainPage', ['$scope', '$rootScope', 'Network', function($scope, $rootScope, Network) {
    //peticion de los videos recientes
    Network.getUrl("/videos/:recientes").success(function(data){
       $scope.videosRecientes = data;
    });
    //peticion de los videos mas visitados
    Network.getUrl("/videos/:visitados").success(function(data){
       $scope.videosMasVisitados = data;
    });
}]);

webApp.controller('seleccionNacho', ['$scope', '$rootScope', 'Network', function($scope, $rootScope, Network) {
    //peticion de los videos filtrados
    $scope.twoThingsSelected = false;

    $scope.loadRowVideos = function (title, ValueNoSelected, flag) {
        $scope.rowVideos = [];
        $scope.twoThingsSelected = false;
        var k = 0;
        var v = 0;
        var t = 0;
        for (var i = 0; i < ValueNoSelected.length; i++) {
            for (var j = 0; j < ValueNoSelected[i].rows.length; j++) {
                if (ValueNoSelected[i].rows[j].type != "text") {
                    $scope.rowVideos[v] = {};
                    if(v === 0) {
                        $scope.rowVideos[v].title = ValueNoSelected[0].title;
                    }
                    if(ValueNoSelected[i].title != $scope.rowVideos[t].title) {
                        $scope.rowVideos[v].title = ValueNoSelected[i].title;
                        t = v;
                    } else {
                        $scope.rowVideos[v].title = $scope.rowVideos[t].title;
                    }
                    $scope.rowVideos[v].titleVideos = ValueNoSelected[i].rows[j].name;
                    if (flag) {
                        var titleSelected = ValueNoSelected[i].title + " " + ValueNoSelected[i].rows[j].name + "" + title;
                    } else {
                       var titleSelected = title + "" + ValueNoSelected[i].title + " " + ValueNoSelected[i].rows[j].name;
                    }
                    Network.getUrl("/videos/:title=" + string_to_slug(titleSelected)).success(function(data){
                        $scope.rowVideos[k].videos = [];
                        $scope.rowVideos[k].videos = data;
                        k = k + 1;
                    });
                    v = v + 1;
                }
            }
        }
    };

    $rootScope.$on('selectSideBar', function(event, props){
        if (props.level && props.asignatura) {
            $scope.twoThingsSelected = true;
            Network.getUrl("/videos/:title=" + string_to_slug(props.selectedTitle)).success(function(data){
               $scope.videos = data;
            });
        } else if (props.level) {
            $scope.loadRowVideos(props.selectedTitle, props.asignaturas);
        } else if (props.asignatura) {
            $scope.loadRowVideos(props.selectedTitle, props.levels, true);
        }
    });
}]);

webApp.factory('Network', function($http) {
    return {
        getUrl: function(url) {
            if (token) {
                return $http({
                    url: url,
                    method: 'GET',
                    headers: {
                      'x-access-token': token
                    }
                }) 
            } else {
                return $http({
                    url: url,
                    method: 'GET'
                })  
            }
        },
        postUrl: function(url, data) {
            if (token) {
                return $http({
                    url: url,
                    data: data,
                    method: 'POST',
                    headers: {
                      'x-access-token': token
                    }
                }) 
            } else {
                return $http({
                    url: url,
                    data: data,
                    method: 'POST'
                })  
            }
        }
    }
});

webApp.directive('mainNacho', function() {
    return {
        templateUrl: '/static/directivas/main-nacho.html',
        restrict: 'EA'
    };
});

webApp.directive('dropdownNacho', function() {
    return {
        templateUrl: '/static/directivas/dropdown-nacho.html',
        scope: {
            select: "&function",
            title: "@title"
        },
        restrict: 'EA',
        link: function (scope, element, attrs) {
            scope.rows = JSON.parse(attrs.rows);
            scope.select({name: name, title: scope.title});
        }
    };
});

webApp.directive('videosNacho', function() {
    return {
        templateUrl: '/static/directivas/videos-nacho.html',
        scope: {
            title: "@title",
            url: "@url",
            urlimagen: "@urlimagen",
            ids: "@ids"
        },
        restrict: 'EA',
        link: function (scope, element, attrs) {
            scope.selectVideo = function () {
                scope.$emit('selectSideBar', {
                    selectedTitle: scope.title
                });
                scope.$emit('selectVideo', {
                    idVideo: scope.ids,
                    urlVideo: scope.url
                });
            }
        }
    };
});

webApp.directive('videoNacho', function($sce) {
    return {
        templateUrl: '/static/directivas/video-nacho.html',
        scope: {
            url: "@url",
            id: "@id"
        },
        restrict: 'EA',
        link: function (scope, element, attrs) {
            scope.trustSrc = function(src) {
                return $sce.trustAsResourceUrl(src);
            }
        }
    };
});

webApp.directive('seleccionNacho', function($rootScope) {
    return {
        templateUrl: '/static/directivas/seleccion-nacho.html',
        scope: {
            title: "@title",
        },
        restrict: 'EA',
        link: function (scope, element, attrs) {
            scope.videoNoSelected = true;
            $rootScope.$on('selectVideo', function(event, props){
                scope.videoNoSelected = false;
                scope.video = {};
                scope.video._id = props.idVideo;
                scope.video.url = props.urlVideo;
            });
            $rootScope.$on('selectSideBar', function(event, props){
                scope.title = props.selectedTitle;
                if (scope.title != "undefined" && scope.title != "") {
                    scope.videoNoSelected = true;
                }
            });
        }
    };
});

webApp.directive('videosCarrete', function($rootScope) {
    return {
        templateUrl: '/static/directivas/videos-carrete.html',
        restrict: 'EA',
        link: function (scope, element, attrs) {
            scope.videos = JSON.parse(attrs.videos);
            scope.moveLeft = function(name) {
                var margenCalculado = document.getElementById(name).style.marginLeft;
                if (margenCalculado == "") {
                    margenCalculado = 0;
                } else {
                    margenCalculado = parseInt(margenCalculado);
                }
                if (margenCalculado < 0) {
                    if (margenCalculado < -20) {
                        $("#" + name).animate({"margin-left": margenCalculado + 20 + "%"},400);
                    } else {
                        $("#" + name).animate({"margin-left": 0 + "%"},400);
                    }
                } 
            };
            scope.moveRight = function(name) {
                var margenCalculado = document.getElementById(name).style.marginLeft;
                if (margenCalculado == "") {
                    margenCalculado = 0;
                } else {
                    margenCalculado = parseInt(margenCalculado);
                }
                if (margenCalculado > -100) {
                    if (margenCalculado > -80) {
                        $("#" + name).animate({"margin-left": margenCalculado - 20 + "%"},400);
                    } else {
                        $("#" + name).animate({"margin-left": -100 + "%"},400);
                    }
                }
            };
        }
    };
});

webApp.directive('entrarPageNacho', function($sce, Network, $rootScope) {
    return {
        templateUrl: '/static/directivas/entrar-page-nacho.html',
        scope: {},
        restrict: 'EA',
        link: function (scope, element, attrs) {
            scope.errorMessage = "";

            scope.entrar = function (user, password) {
                data = {
                    email: user,
                    password: password
                };
                if (user) {
                    if (password) {
                        scope.errorMessage = "";
                        Network.postUrl("/logginUser", data).success(function(data){
                            token = data.token;
                            localStorage.setItem('token',token);
                            localStorage.setItem('user',JSON.stringify(data.user));
                            scope.$emit('logginUserOk', {
                                user: data.user
                            })
                        }).error (function(response) {
                            if (response == "Bad Request") {
                                scope.errorMessage = "El email introducido no es correcto";
                            } else if (response == "Unauthorized") {
                                scope.errorMessage = "La contraseña introducida no es correcta";
                            }
                        });
                    } else {
                        scope.errorMessage = "La contraseña no puede estar vacia";
                    }
                } else {
                    scope.errorMessage = "El email no es valido";
                }
            }
        }
    };
});

webApp.directive('registrarPageNacho', function($sce, $rootScope, Network) {
    return {
        templateUrl: '/static/directivas/registrar-page-nacho.html',
        scope: {},
        restrict: 'EA',
        link: function (scope, element, attrs) {
            scope.selectedRegisterType = "";
            scope.registroRealizado = false;
            scope.mensaje = "";

            scope.changeSelectedRegisterType = function(selection) {
                scope.selectedRegisterType = selection;
            };

            $rootScope.$on('registrarClickEvent', function(event, props){
                scope.selectedRegisterType = "";
                scope.registroRealizado = false;
            });

            scope.registrarUser = function (name, subname, email, password, password2) {
                var newUser = {
                    name: name,
                    subname: subname,
                    email: email,
                    password: password
                };
                if(name) {
                    if (subname) {
                        if (password) {
                            if (password === password2) {
                                if (email) {
                                    scope.mensaje = "";
                                    Network.postUrl("/adduser", newUser).success(function(data){});
                                    scope.registroRealizado = true;
                                } else {
                                    scope.mensaje = "El correo electronico es incorrecto";
                                }
                            } else {
                                scope.mensaje = "Las contraseñas no coinciden";
                            }
                        } else {
                            scope.mensaje = "La contraseña no puede estar vacia";
                        }
                    } else {
                        scope.mensaje = "El apellido no puede estar vacio";
                    }
                } else {
                    scope.mensaje = "El nombre no puede estar vacio";
                }
            };
        }
    };
});